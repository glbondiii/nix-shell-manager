#!/bin/sh

function help_message() {
    echo "nix-shell-manager options: 'flake/shell' 'add/remove/edit/get/list'"
    echo "flake: uses nix flakes for the shells (default behavior)"
    echo "shell: specifies using base nix shells (legacy behavior)"
    echo "add: create a new nix-shell profile; first argument: name of profile, second argument: path to existing file which you want to be your shell file for the profile"
    echo "remove: removes existing nix-shell profile; first argument: name of profile"
    echo "edit: edits existing nix-shell profile; first argument: name of profile"
    echo "get: gets an existing nix-shell profile or if none is provided or found, returns an empty nix-shell file; first argument: name of profile"
    echo "list: list all available nix-shell profiles"
}

if [ -z $NSM_DIR ]; then
    nsm_dir="$HOME/nix-shell-manager"
else
    nsm_dir="$NSM_DIR"
fi

script_dir="$nsm_dir/scripts"

if [ -z $NIX_SHELL_DIR ]; then
    nix_shell_dir="$HOME/.nix-shells"
else
    nix_shell_dir="$NIX_SHELL_DIR"
fi

if [ ! -d "$nix_shell_dir" ] ; then
    mkdir -p $nix_shell_dir
    cp -r "$nsm_dir/flakes" $nix_shell_dir
    cp -r "$nsm_dir/shells" $nix_shell_dir
fi

if [ -z $1 ]
then
    input="help"
elif [ $1 = "flake" ] || [ $1 = "shell" ]
then
    get=$1
    input=$2
    arg1=$3
    arg2=$4
else
    get="flake"
    input=$1
    arg1=$2
    arg2=$3
fi

if [ $input = "add" ]
then
    $script_dir/add_nix_${get}.sh $arg1 $arg2
elif [ $input = "remove" ]
then
    $script_dir/remove_nix_${get}.sh $arg1
elif [ $input = "edit" ]
then
    $script_dir/edit_nix_${get}.sh $arg1
elif [ $input = "get" ]
then
    $script_dir/get_nix_${get}.sh $arg1
elif [ $input = "list" ]
then
    $script_dir/list_nix_${get}s.sh
else
    help_message
fi

