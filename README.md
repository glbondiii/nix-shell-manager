# nix-shell-manager
Shell scripts to manage your nix-shells   

## How to use:
1. Clone this repo into your home folder and alias main.sh to "nix-shell-manager"
2. Make main.sh executable and run it with any of the below arguments
* add: create a new nix-shell profile; first argument: name of profile, second argument: path to existing file
which you want to be your shell file for the profile
* remove: removes existing nix-shell profile; first argument: name of profile
* edit: edits existing nix-shell profile; first argument: name of profile
* get: gets an existing nix-shell profile or if none is provided or found, returns an empty nix-shell file
first argument: name of profile
* list: list all available nix-shell profiles

## Shells Directory
Contains various shell templates that I have made for various reasons   
Includes 2 nix-ld shells, one of which is inspired by tobiasBora's sample config (source: https://unix.stackexchange.com/questions/522822/different-methods-to-run-a-non-nixos-executable-on-nixos)   
A minimal nix-ld shell is also provided
