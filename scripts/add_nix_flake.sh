#!/bin/sh

if [ -z $NIX_SHELL_DIR ]; then
    nsm_dir="$HOME/.nix-shells"
else
    nsm_dir="$NIX_SHELL_DIR"
fi

if [ -z "$1" ]
then 
	input="NEW-FLAKE"
else
	input=$1
fi

if [ -z "$2" ]
then
    flake_path="flake.nix"
    flake="flake.nix"
else
    flake_path=$(pwd)/$2
    flake=$2
fi

if [ -z "$3" ]
then
    env_path="envrc"
    env="envrc"
else
    env_path=$(pwd)/$3
    env=$3
fi

cd $nsm_dir/flakes

mkdir -p $input
cp $flake_path $input
cp $env_path $input

"${EDITOR:-vim}" $input/$flake
echo "$input flake profile created"

cd - > /dev/null
