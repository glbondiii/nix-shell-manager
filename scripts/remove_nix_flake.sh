#!/bin/sh

if [ -z $NIX_SHELL_DIR ]; then
    nsm_dir="$HOME/.nix-shells"
else
    nsm_dir="$NIX_SHELL_DIR"
fi

if [ "$1" ]
then 
    rm -rf "$nsm_dir/flakes/$1"
else
    echo "No profile provided"
fi

