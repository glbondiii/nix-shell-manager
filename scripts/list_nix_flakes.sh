#!/bin/sh

if [ -z $NIX_SHELL_DIR ]; then
    nsm_dir="$HOME/.nix-shells"
else
    nsm_dir="$NIX_SHELL_DIR"
fi

cd $nsm_dir/flakes
echo "Current List of Nix Flakes:"
set -- */; printf "%s\n" "${@%/}"
cd - > /dev/null
