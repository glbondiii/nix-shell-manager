#!/bin/sh

if [ -z $NIX_SHELL_DIR ]; then
    nsm_dir="$HOME/.nix-shells"
else
    nsm_dir="$NIX_SHELL_DIR"
fi

if [ -z "$1" ]
then 
	input="NEW-SHELL"
else
	input=$1
fi

if [ -z "$2" ]
then
    shell_path="shell.nix"
    shell="shell.nix"
else
    shell_path=$(pwd)/$2
    shell=$2
fi

cd $nsm_dir/shells

mkdir -p $input
cp $shell_path $input

"${EDITOR:-vim}" $input/$shell
echo "$input shell profile created"

cd - > /dev/null
