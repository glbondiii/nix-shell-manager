#!/bin/sh

if [ -z $NIX_SHELL_DIR ]; then
    nsm_dir="$HOME/.nix-shells"
else
    nsm_dir="$NIX_SHELL_DIR"
fi

cd $nsm_dir/shells
echo "Current List of Nix Shells:"
set -- */; printf "%s\n" "${@%/}"
cd - > /dev/null
