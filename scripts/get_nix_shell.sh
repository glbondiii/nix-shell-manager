#!/bin/sh

if [ -z $NIX_SHELL_DIR ]; then
    nsm_dir="$HOME/.nix-shells"
else
    nsm_dir="$NIX_SHELL_DIR"
fi

if [ "$1" ]
then 
    input=/$1
fi

cp "$nsm_dir/shells$input/shell.nix" ./shell.nix
